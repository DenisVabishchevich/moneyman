**Endpoints:**

- Create Pipeline:

*POST http://localhost:8080/api/v1.0/pipelines/*

```
{
      "status": "PENDING",
      "name": "Simple Pipeline",
      "tasks": [
          {
              "taskName": "BUILD",
              "description": "desc",
              "action": {
                  "@type": "Random",
                  "status": "PENDING"
              }
          },
          {
              "taskName": "DOCS",
              "description": "desc",
              "action": {
                  "@type": "Delayed",
                  "status": "PENDING"
              }
          }
      ]
  }
``` 

- Get all pipelines

*GET http://localhost:8080/api/v1.0/pipelines/*

- Get pipeline by ID

*GET http://localhost:8080/api/v1.0/pipelines/{id}*

- Update pipeline

*PUT http://localhost:8080/api/v1.0/pipelines/{id}*

- Delete pipeline

*DELETE http://localhost:8080/api/v1.0/pipelines/{id}*

**EXECUTE PIPELINE**

GET http://localhost:8080/execute?id=1

**STOP PIPELINE**

GET http://localhost:8080/stop?id=1

**SHOW PIPELINE STATUS**

GET http://localhost:8080/show?id=1

**How to run:**

-  git clone https://DenisVabishchevich@bitbucket.org/DenisVabishchevich/moneyman.git
-  cd moneyman
- ./mvnw spring-boot:run
