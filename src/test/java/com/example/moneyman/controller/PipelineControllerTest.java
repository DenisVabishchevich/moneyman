package com.example.moneyman.controller;

import com.example.moneyman.MoneymanApplication;
import com.example.moneyman.domain.Pipeline;
import com.example.moneyman.domain.Task;
import com.example.moneyman.domain.TaskName;
import com.example.moneyman.domain.actions.Delayed;
import com.example.moneyman.domain.actions.Random;
import com.example.moneyman.service.PipelineService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = MoneymanApplication.class)
@WebAppConfiguration
public class PipelineControllerTest {

    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    private MockMvc mockMvc;

    private HttpMessageConverter mappingJackson2HttpMessageConverter;

    @Autowired
    private PipelineService service;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {

        this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream().filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter).findAny().orElse(null);

        assertNotNull("the JSON message converter must not be null", this.mappingJackson2HttpMessageConverter);
    }

    @Before
    public void setup() throws Exception {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void createPipelineTest() throws Exception {
        String pipelineJson = json(getPipelineObject());
        this.mockMvc.perform(post("/api/v1.0/pipelines")
                .contentType(contentType)
                .content(pipelineJson))
                .andExpect(status().isCreated());
    }

    @Test
    public void getPipelineTest() throws Exception {
        Pipeline saved = service.createPipeline(getPipelineObject());
        String pipelineJson = json(saved);
        this.mockMvc.perform(get("/api/v1.0/pipelines/" + saved.getId())
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().json(pipelineJson));
    }

    @Test
    public void deletePipelineTest() throws Exception {
        Pipeline saved = service.createPipeline(getPipelineObject());
        this.mockMvc.perform(delete("/api/v1.0/pipelines/" + saved.getId())
                .contentType(contentType))
                .andExpect(status().isOk());
        assertTrue(!service.getPipelineById(saved.getId()).isPresent());
    }

    @Test
    public void updatePipelineTest() throws Exception {
        Pipeline saved = service.createPipeline(getPipelineObject());
        Long id = saved.getId();

        saved.setId(null);
        saved.setName("changed");
        String pipelineJson = json(saved);

        saved.setId(id);
        String pipelineJsonWithId = json(saved);

        this.mockMvc.perform(put("/api/v1.0/pipelines/" + id)
                .contentType(contentType)
                .content(pipelineJson))
                .andExpect(status().isOk())
                .andExpect(content().json(pipelineJsonWithId));

    }

    protected String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }

    private Pipeline getPipelineObject() {
        Pipeline pipeline = new Pipeline();
        Task task1 = new Task();
        task1.setDescription("desc");
        task1.setTaskName(TaskName.BUILD);
        Random random = new Random();
        task1.setAction(random);
        Task task2 = new Task();
        task2.setDescription("desc");
        task2.setTaskName(TaskName.DOCS);
        Delayed delayed = new Delayed();
        task2.setAction(delayed);
        List<Task> tasks = Arrays.asList(task1, task2);
        pipeline.setTasks(tasks);
        return pipeline;
    }
}