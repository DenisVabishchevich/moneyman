package com.example.moneyman.repository;

import com.example.moneyman.domain.Task;
import com.example.moneyman.domain.TaskName;
import com.example.moneyman.domain.actions.Action;
import com.example.moneyman.domain.actions.Random;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Objects;

import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
@DataJpaTest
public class TaskRepositoryTest {

    @Autowired
    private TaskRepository repo;

    @Test
    public void saveTaskTest() {
        Task task = new Task();
        task.setDescription("desc");
        task.setTaskName(TaskName.BUILD);
        Action random = new Random();
        task.setAction(random);
        Task savedTask = repo.save(task);

        assertTrue(Objects.nonNull(savedTask.getId()));
    }

}