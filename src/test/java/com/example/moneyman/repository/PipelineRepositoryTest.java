package com.example.moneyman.repository;

import com.example.moneyman.domain.Pipeline;
import com.example.moneyman.domain.Task;
import com.example.moneyman.domain.TaskName;
import com.example.moneyman.domain.actions.Delayed;
import com.example.moneyman.domain.actions.Random;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
@DataJpaTest
@Transactional
public class PipelineRepositoryTest {

    @Autowired
    EntityManager manager;
    @Autowired
    private PipelineRepository repo;

    @Test
    public void savePipeline() throws Exception {
        Pipeline pipeline = new Pipeline();
        Task task1 = new Task();
        task1.setDescription("desc");
        task1.setTaskName(TaskName.BUILD);
        Random random = new Random();
        task1.setAction(random);
        Task task2 = new Task();
        task2.setDescription("desc");
        task2.setTaskName(TaskName.DOCS);
        Delayed delayed = new Delayed();
        task2.setAction(delayed);
        List <Task> tasks = Arrays.asList(task1, task2);
        pipeline.setTasks(tasks);
        Pipeline savedPipeline = repo.save(pipeline);
        assertTrue(Objects.nonNull(savedPipeline.getId()));
    }

}