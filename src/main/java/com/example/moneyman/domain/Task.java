package com.example.moneyman.domain;

import com.example.moneyman.domain.actions.Action;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "TASK")
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Task implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "NAME")
    @Enumerated(EnumType.STRING)
    private TaskName taskName;

    @Column(name = "DESC")
    private String description;

    @Transient
    private LocalDateTime startDate;

    @Transient
    private LocalDateTime endDate;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Action action;

    public Status execute() throws Exception {
        startDate = LocalDateTime.now();
        Status status = action.processTask(taskName);
        endDate = LocalDateTime.now();
        return status;
    }

}
