package com.example.moneyman.domain.actions;

import com.example.moneyman.domain.TaskName;
import com.example.moneyman.domain.Status;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.transaction.NotSupportedException;
import java.io.Serializable;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@ToString
@Getter
@Setter
@NoArgsConstructor
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "@type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = Completed.class, name = "Completed"),
        @JsonSubTypes.Type(value = Delayed.class, name = "Delayed"),
        @JsonSubTypes.Type(value = Print.class, name = "Print"),
        @JsonSubTypes.Type(value = Random.class, name = "Random")
})
public abstract class Action implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "TASK_STATUS")
    @Enumerated(EnumType.STRING)
    protected Status status = Status.PENDING;

    public abstract Status processTask(TaskName taskName) throws Exception;
}


