package com.example.moneyman.domain.actions;

import com.example.moneyman.domain.TaskName;
import com.example.moneyman.domain.Status;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.List;

@ToString
@Getter
@Setter
@NoArgsConstructor
@Entity
public class Random extends Action {

    @Transient
    private static final SecureRandom RANDOM = new SecureRandom();

    @Transient
    private static final List<Status> VALUES = Arrays.asList(Status.values());

    @Transient
    private static final int SIZE = VALUES.size();

    @Override
    public Status processTask(TaskName taskName) throws InterruptedException {
        status = Status.IN_PROGRESS;
        System.out.println(taskName.toString());
        Thread.currentThread().sleep(1000);
        status = VALUES.get(RANDOM.nextInt(VALUES.size()));
        return status;
    }
}
