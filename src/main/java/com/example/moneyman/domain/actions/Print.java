package com.example.moneyman.domain.actions;

import com.example.moneyman.domain.TaskName;
import com.example.moneyman.domain.Status;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@ToString
@Getter
@Setter
@NoArgsConstructor
@Entity
public class Print extends Action {

    @Override
    public Status processTask(TaskName taskName) {
        status = Status.IN_PROGRESS;
        System.out.println(taskName.toString());
        status = Status.COMPLETED;
        return status;
    }
}
