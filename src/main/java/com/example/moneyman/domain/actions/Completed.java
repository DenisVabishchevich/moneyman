package com.example.moneyman.domain.actions;

import com.example.moneyman.domain.TaskName;
import com.example.moneyman.domain.Status;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@ToString
@Getter
@Setter
@NoArgsConstructor
@Entity
public class Completed extends Action {

    @Override
    public Status processTask(TaskName taskName) throws InterruptedException {
        status = Status.IN_PROGRESS;
        System.out.println(taskName.toString());
        Thread.currentThread().sleep(1000);
        status = Status.COMPLETED;
        return status;
    }
}
