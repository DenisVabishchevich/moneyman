package com.example.moneyman.domain;

public enum TaskName {
    DOCS,
    TEST,
    INTEGRATION_TEST,
    SYNC,
    BUILD,
    PUBLISH
}
