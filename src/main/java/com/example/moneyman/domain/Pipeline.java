package com.example.moneyman.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "EXECUTION")
@NoArgsConstructor
@Setter
@Getter
@ToString
public class Pipeline implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "EXECUTION_STATUS")
    @Enumerated(EnumType.STRING)
    private volatile Status status = Status.PENDING;

    @Column(name = "NAME", unique = true)
    private String name;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Task> tasks = new ArrayList<>();

    @Transient
    private LocalDateTime startDate;

    @Transient
    private LocalDateTime endDate;

    @Transient
    @JsonIgnore
    private Thread thread;

    public void execute() throws Exception {
        startDate = LocalDateTime.now();
        status = Status.IN_PROGRESS;

        Runnable r = () -> {
            for (Task task : getTasks()) {

                try {
                    if (task.execute() == Status.FAILED) {
                        status = Status.FAILED;
                        status = Status.FAILED;
                        return;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    status = Status.FAILED;
                    return;
                }
            }
            endDate = LocalDateTime.now();
            status = Status.COMPLETED;
        };
        thread = new Thread(r);
        thread.start();
    }

    public void stop() {
        if (thread != null) {
            thread.interrupt();
        }
    }
}
