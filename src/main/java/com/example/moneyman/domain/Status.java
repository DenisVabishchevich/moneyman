package com.example.moneyman.domain;

public enum Status {
    PENDING,
    IN_PROGRESS,
    SKIPPED,
    FAILED,
    COMPLETED
}
