package com.example.moneyman.controller;

import com.example.moneyman.domain.Pipeline;
import com.example.moneyman.service.PipelineService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1.0/pipelines")
public class PipelineController {

    private PipelineService service;

    public PipelineController(PipelineService service) {
        this.service = service;
    }

    @GetMapping
    public ResponseEntity <List <Pipeline>> getAll() {
        return ResponseEntity.ok(service.getAllPipelines());
    }

    @GetMapping("/{id}")
    public ResponseEntity <Pipeline> getPipeline(@PathVariable Long id) {
        Optional <Pipeline> byId = service.getPipelineById(id);
        return byId.isPresent() ? ResponseEntity.ok(byId.get()) : ResponseEntity.notFound().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity <Void> deletePipeline(@PathVariable Long id) {
        service.deletePipeline(id);
        return ResponseEntity.ok().build();
    }

    @PostMapping
    public ResponseEntity <Pipeline> createPipeline(@RequestBody Pipeline pipeline) {
        Pipeline saved = service.createPipeline(pipeline);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(saved.getId()).toUri();
        return ResponseEntity.created(location).body(saved);
    }

    @PutMapping("/{id}")
    public ResponseEntity <Pipeline> updatePipeline(@PathVariable Long id, @RequestBody Pipeline pipeline) {
        Optional <Pipeline> fromDb = service.getPipelineById(id);
        if (!fromDb.isPresent()) {
            return ResponseEntity.notFound().build();
        } else {
            pipeline.setId(id);
            Pipeline saved = service.updatePipeline(pipeline);
            return ResponseEntity.ok(saved);
        }

    }
}
