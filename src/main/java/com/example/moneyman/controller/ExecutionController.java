package com.example.moneyman.controller;

import com.example.moneyman.domain.Pipeline;
import com.example.moneyman.service.PipelineService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@RestController
public class ExecutionController {

    private static Map<Long, Pipeline> activePipelines = new HashMap<>();
    private PipelineService service;

    public ExecutionController(PipelineService service) {
        this.service = service;
    }

    @GetMapping("/execute")
    public ResponseEntity<Pipeline> execute(HttpServletRequest request) throws Exception {
        System.out.println("id is " + request.getParameter("id"));
        Optional<Pipeline> opt = service.getPipelineById(Long.valueOf(request.getParameter("id")));

        if (opt.isPresent()) {
            activePipelines.put(opt.get().getId(), opt.get());
            opt.get().execute();
            return ResponseEntity.ok(opt.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/show")
    public ResponseEntity<Pipeline> show(HttpServletRequest request) throws Exception {
        return ResponseEntity.ok(activePipelines.get(Long.valueOf(request.getParameter("id"))));
    }

    @GetMapping("/stop")
    public ResponseEntity<Pipeline> stop(HttpServletRequest request) throws Exception {
        Pipeline p = activePipelines.get(Long.valueOf(request.getParameter("id")));
        p.stop();
        return ResponseEntity.ok(p);
    }


}
