package com.example.moneyman.repository;

import com.example.moneyman.domain.Pipeline;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PipelineRepository extends JpaRepository<Pipeline, Long> {

    Optional<Pipeline> findPipelineByName(String name);
}
