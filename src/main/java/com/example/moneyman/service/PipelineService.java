package com.example.moneyman.service;

import com.example.moneyman.domain.Pipeline;

import java.util.List;
import java.util.Optional;

public interface PipelineService {

    void deletePipeline(Long id);

    Optional<Pipeline> getPipelineById(Long id);

    List<Pipeline> getAllPipelines();

    Pipeline createPipeline(Pipeline pipeline);

    Pipeline updatePipeline(Pipeline pipeline);

    Optional<Pipeline> findPipelineByName(String name);

}
