package com.example.moneyman.service;

import com.example.moneyman.domain.Pipeline;
import com.example.moneyman.repository.PipelineRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class PipelineServiceDb implements PipelineService {

    private PipelineRepository repository;

    public PipelineServiceDb(PipelineRepository repository) {
        this.repository = repository;
    }

    @Override
    public void deletePipeline(Long id) {
        repository.deleteById(id);
    }

    @Override
    public Optional<Pipeline> getPipelineById(Long id) {
        return repository.findById(id);
    }

    @Override
    public List<Pipeline> getAllPipelines() {
        return repository.findAll();
    }

    @Override
    public Pipeline createPipeline(Pipeline pipeline) {
        return repository.save(pipeline);
    }

    @Override
    public Pipeline updatePipeline(Pipeline pipeline) {
        return repository.save(pipeline);
    }

    @Override
    public Optional<Pipeline> findPipelineByName(String name) {
        return repository.findPipelineByName(name);
    }
}
